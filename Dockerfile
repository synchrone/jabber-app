FROM cloudron/base:3.2.0@sha256:ba1d566164a67c266782545ea9809dc611c4152e27686fd14060332dd88263ea

WORKDIR /app/code
CMD [ "/app/code/start.sh" ]

RUN echo deb http://packages.prosody.im/debian $(lsb_release -sc) main > /etc/apt/sources.list.d/prosody.list && \
    wget https://prosody.im/files/prosody-debian-packages.key -O- | apt-key add - && \
    apt-get update && \
    apt-get -y install prosody-0.12 lua-sec lua-luaossl lua-ldap lua-dbi-sqlite3 mercurial && \
    hg clone https://hg.prosody.im/prosody-modules/ /usr/local/prosody-modules && \
    apt-get -y remove mercurial && \
    apt-get -y autoremove && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/* && \
    ln -sf /app/code/prosody.cfg.lua /etc/prosody/prosody.cfg.lua

RUN mkdir frontend && \
    curl -sL https://github.com/conversejs/converse.js/releases/download/v10.0.0/converse.js-10.0.0.tgz | \
    tar --strip-components=1 -C frontend -xvzf - package/dist/ && \
    curl -sL https://github.com/signalapp/libsignal-protocol-javascript/archive/refs/tags/v1.2.0.tar.gz | \
    tar --strip-components=2 -C frontend -xvzf - libsignal-protocol-javascript-1.2.0/dist/libsignal-protocol.js

COPY . .

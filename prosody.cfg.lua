admins = { }
plugin_paths = {"/usr/local/prosody-modules"} -- https://modules.prosody.im/

-- Documentation for bundled modules can be found at: https://prosody.im/doc/modules
modules_enabled = {
    -- Generally required
    "disco"; -- Service discovery
    "roster"; -- Allow users to have a roster. Recommended ;)
    "saslauth"; -- Authentication for clients and servers. Recommended if you want to log in.
    "tls"; -- Add support for secure TLS on c2s/s2s connections

    -- Not essential, but recommended
    "blocklist"; -- Allow users to block communications with other users
    "bookmarks"; -- Synchronise the list of open rooms between clients
    "carbons"; -- Keep multiple online clients in sync
    "dialback"; -- Support for verifying remote servers using DNS
    "limits"; -- Enable bandwidth limiting for XMPP connections
    "pep"; -- Allow users to store public and private data in their account
    "private"; -- Legacy account storage mechanism (XEP-0049)
    "smacks"; -- Stream management and resumption (XEP-0198)
    "vcard4"; -- User profiles (stored in PEP)
    "vcard_legacy"; -- Conversion between legacy vCard and PEP Avatar, vcard

    -- Nice to have
    "csi_simple"; -- Simple but effective traffic optimizations for mobile devices
    --"invites"; -- Create and manage invites
    --"invites_adhoc"; -- Allow admins/users to create invitations via their client
    --"invites_register"; -- Allows invited users to create accounts
    "ping"; -- Replies to XMPP pings with pongs
    --"register"; -- Allow users to register on this server using a client and change passwords
    "time"; -- Let others know the time here on this server
    --"uptime"; -- Report how long server has been running
    "version"; -- Replies to server version requests
    "mam"; -- Store recent messages to allow multi-device synchronization
    "turn_external"; -- Provide external STUN/TURN service for e.g. audio/video calls

    -- Admin interfaces
    --"admin_adhoc"; -- Allows administration via an XMPP client that supports ad-hoc commands
    "admin_shell"; -- Allow secure administration via 'prosodyctl shell'

    -- HTTP modules
    "http_files"; -- Serve static assets
    "bosh"; -- Enable BOSH clients, aka "Jabber over HTTP"
    --"http_openmetrics"; -- for exposing metrics to stats collectors
    "websocket"; -- XMPP over WebSockets

    -- Other specific functionality
    --"announce"; -- Send announcement to all online users
    --"groups"; -- Shared roster support
    --"legacyauth"; -- Legacy authentication. Only used by some old clients and bots.
    --"mimicking"; -- Prevent address spoofing
    --"motd"; -- Send a message to users when they log in
    "proxy65"; -- Enables a file transfer proxy service which clients behind NAT can use
    "s2s_bidi"; -- Bi-directional server-to-server (XEP-0288)
    --"server_contact_info"; -- Publish contact information for this service
    --"tombstones"; -- Prevent registration of deleted accounts
    --"watchregistrations"; -- Alert admins of registrations
    --"welcome"; -- Welcome users who register accounts

    -- Community modules
    "privacy_lists",
    "blocking", -- XEP-0191
    "cloud_notify", -- XEP-0357
    "cloud_notify_extensions", -- XEP-0357+
    "s2s_keepalive", -- XEP-0199
}

-- These modules are auto-loaded, but should you want
-- to disable them then uncomment them here:
modules_disabled = {
    -- "offline"; -- Store offline messages
    -- "c2s"; -- Handle client connections
    -- "s2s"; -- Handle server-to-server connections
    -- "posix"; -- POSIX functionality, sends server to background, etc.
}

-- For more information see https://prosody.im/doc/creating_accounts
allow_registration = false -- using LDAP

c2s_require_encryption = true
c2s_ports = { tonumber(os.getenv('C2S_PORT')) }
c2s_direct_tls_ports = { tonumber(os.getenv('C2STLS_PORT')) }

s2s_secure_auth = true
s2s_require_encryption = true
s2s_ports = { tonumber(os.getenv('S2S_PORT')) }
s2s_direct_tls_ports = { tonumber(os.getenv('S2STLS_PORT')) }

pidfile = "/app/data/prosody.pid"
admin_socket = "/app/data/prosody.sock"

limits = {
    c2s = {
        rate = "10kb/s";
    };
    s2sin = {
        rate = "30kb/s";
    };
}

-- Authentication
-- For more information see https://prosody.im/doc/authentication
authentication = "ldap" -- https://docs.cloudron.io/packaging/addons/#ldap
ldap_base = os.getenv("CLOUDRON_LDAP_USERS_BASE_DN")
ldap_server = os.getenv("CLOUDRON_LDAP_SERVER") .. ":" .. os.getenv("CLOUDRON_LDAP_PORT")
ldap_rootdn = os.getenv("CLOUDRON_LDAP_BIND_DN")
ldap_password = os.getenv("CLOUDRON_LDAP_BIND_PASSWORD")
ldap_filter   = "(&(objectclass=user)(|(username=$user)(mail=$user@$host)))"

-- Storage
-- See https://prosody.im/doc/storage for more info.
storage = "sql" -- Default is "internal"
sql = { driver = "SQLite3", database = "/app/data/prosody.sqlite" } -- https://docs.cloudron.io/packaging/addons/#localstorage

-- Archiving configuration
-- If mod_mam is enabled, Prosody will store a copy of every message. This
-- is used to synchronize conversations between multiple clients, even if
-- they are offline. This setting controls how long Prosody will keep
-- messages in the archive before removing them.

archive_expires_after = "1w" -- Remove archived messages after 1 week

-- You can also configure messages to be stored in-memory only. For more
-- archiving options, see https://prosody.im/doc/modules/mod_mam

-- Audio/video call relay (STUN/TURN)
-- Find more information at https://prosody.im/doc/turn
turn_external_secret = os.getenv('CLOUDRON_TURN_SECRET')
turn_external_host = os.getenv('CLOUDRON_TURN_SERVER')
turn_external_port = os.getenv('CLOUDRON_TURN_PORT')
turn_external_tls_port = os.getenv('CLOUDRON_TURN_TLS_PORT')

-- Logging configuration
-- For advanced logging see https://prosody.im/doc/logging
log = {
    { levels = { min = "info", max = "error" }, to = "console" };
    --{ levels = { min = "debug", max = "debug" }, to = "console" }; -- delete me
}

certificates = "/app/data/certs" -- see https://prosody.im/doc/certificates
data_path = "/app/data/prosody" -- https://prosody.im/doc/configure

----------- BOSH / Web -----------
local DOMAIN = os.getenv("CLOUDRON_APP_DOMAIN")
http_external_url = "https://" .. DOMAIN .. "/"
http_ports = { 5280 }
http_interfaces = { "*", "::" }
http_default_host = DOMAIN
trusted_proxies = {"127.0.0.1", "10.0.0.0/8", "172.16.0.0/12", "192.168.0.0/16",}
http_paths = {
    files = "/"; -- Serve frontend from the base URL
    bosh = "/http-bind"; -- Serve frontend from the base URL
}
http_files_dir = "/app/code/frontend"
http_cors_override = {
    bosh = {
        enabled = false;
    };
    websocket = {
        enabled = false;
    };
}

proxy65_ports = { os.getenv("P65_PORT") }
http_file_share_size_limit = 512 * 1024 * 1024; -- 512 MiB

----------- Virtual hosts -----------
-- You need to add a VirtualHost entry for each domain you wish Prosody to serve.
-- Settings under each VirtualHost entry apply *only* to that host.

VirtualHost (DOMAIN)
    ssl = {
        certificate = "/etc/certs/tls_cert.pem";
        key = "/etc/certs/tls_key.pem";
    }
    disco_items = {
        { os.getenv("CONFERENCE_DOMAIN"), "conferencing" },
        { os.getenv("PUBSUB_DOMAIN"), "pubsub" },
        { "upload." .. DOMAIN, "file sharing" },
        { "proxy65." .. DOMAIN, "file upload proxy" },
    }

------ Components ------
-- You can specify components to add hosts that provide special services,
-- like multi-user conferences, and transports.
-- For more information on components, see https://prosody.im/doc/components

---Set up a MUC (multi-user chat) room server on conference.example.com:
Component (os.getenv("CONFERENCE_DOMAIN")) "muc"
    --- Store MUC messages in an archive and allow users to access it
    modules_enabled = {
        "muc_mam";
        "muc_local_only";
        "vcard_muc";
        "muc_defaults";
        "muc_offline_delivery";
        "muc_auto_reserve_nicks";
    }
    ssl = {
        certificate = "/etc/certs/" .. os.getenv("CONFERENCE_DOMAIN") .. ".cert";
        key = "/etc/certs/" .. os.getenv("CONFERENCE_DOMAIN") .. ".key";
    }
    restrict_room_creation = "local"
    muc_room_default_allow_member_invites = true
    muc_room_default_persistent = true
    muc_room_default_public = false

--- Set up a file sharing component
Component ("upload." .. DOMAIN) "http_file_share"
    http_host = DOMAIN
    http_file_share_access = {
        DOMAIN, -- anyone with a @DOMAIN address
    }

Component ("proxy65." .. DOMAIN) "proxy65"
    proxy65_address = DOMAIN
    proxy65_acl = { DOMAIN }

Component (os.getenv("PUBSUB_DOMAIN")) "pubsub"
    ssl = {
        certificate = "/etc/certs/" .. os.getenv("PUBSUB_DOMAIN") .. ".cert";
        key = "/etc/certs/" .. os.getenv("PUBSUB_DOMAIN") .. ".key";
    }

Include '/app/data/prosody.cfg.d/*.lua'

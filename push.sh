#!/bin/bash -e
TAG=$(date +'%Y%m%d%H%M')
LOC=${1:-chat}
IMG=synchrone/jabber-app:$TAG
docker build -t $IMG .
docker push $IMG
cloudron update --app $LOC --image $IMG --no-backup

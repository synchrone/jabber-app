<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content="Converse XMPP/Jabber Chat"/>
    <meta name="keywords" content="xmpp chat webchat converse.js" />
    <title>$CLOUDRON_APP_DOMAIN</title>
    <link rel="stylesheet" type="text/css" media="screen" href="/dist/converse.min.css">
    <script src="/dist/converse.min.js" charset="utf-8"></script>
    <script src="/libsignal-protocol.js"></script>
</head>
<body class="converse-fullscreen">
<noscript>You need to enable JavaScript to run the Converse.js chat app.</noscript>
<div id="conversejs-bg"></div>
<script>
  converse.initialize({
    authentication: 'login',
    auto_away: 300,
    auto_reconnect: true,
    bosh_service_url: 'https://$CLOUDRON_APP_DOMAIN/http-bind',
    websocket_url: 'wss://$CLOUDRON_APP_DOMAIN/xmpp-websocket',
    message_archiving: 'always',
    use_emojione: false,
    view_mode: 'fullscreen',
    default_domain: '$CLOUDRON_APP_DOMAIN'
  });
</script>
</body>
</html>

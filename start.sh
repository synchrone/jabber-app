#!/usr/bin/env bash
set -e
# configure converse.js
cat /app/code/frontend/index.html.tpl | envsubst > /app/data/index.html

# configure prosody
chown cloudron:cloudron /app/data/
mkdir -p /app/data/prosody.cfg.d
exec /usr/local/bin/gosu cloudron:cloudron prosody -F
